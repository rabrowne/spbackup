# README - SPBackup #

This is a simple program that allows you to connect to a site, list all the subsites in the collection and then select the ones you wish to backup. It will iterate through the sites and select all the file libraries that are available and then backup to the target folder.

It is by far not perfect, but it does do a job.