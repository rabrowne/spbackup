﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Resources;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("iGAS Backup Utility")]
[assembly: AssemblyDescription("A simple backup utility that connects to the Sharepoint server and allows for the download and storage of all files on the local disk. The files and folder structure are stored into ZIP files, with each site having a separate file.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("iGAS Technology Solutions Ltd.")]
[assembly: AssemblyProduct("iGAS Backup Utility")]
[assembly: AssemblyCopyright("Copyright © iGAS Technology Solutions Ltd. 2012")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("d396b9f6-7d13-4ce4-a16f-efd5eac7cede")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("0.0.1.0")]
[assembly: AssemblyFileVersion("0.0.1.0")]
[assembly: NeutralResourcesLanguageAttribute("en-GB")]
