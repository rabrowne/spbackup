﻿namespace SPBackupTool
{
    partial class CommentsWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comments_text = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.overwrite = new System.Windows.Forms.RadioButton();
            this.major = new System.Windows.Forms.RadioButton();
            this.minor = new System.Windows.Forms.RadioButton();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // comments_text
            // 
            this.comments_text.Location = new System.Drawing.Point(12, 25);
            this.comments_text.Multiline = true;
            this.comments_text.Name = "comments_text";
            this.comments_text.Size = new System.Drawing.Size(351, 110);
            this.comments_text.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Comment:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.overwrite);
            this.groupBox1.Controls.Add(this.major);
            this.groupBox1.Controls.Add(this.minor);
            this.groupBox1.Location = new System.Drawing.Point(12, 141);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(351, 50);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Checkin Type:";
            // 
            // overwrite
            // 
            this.overwrite.AutoSize = true;
            this.overwrite.Location = new System.Drawing.Point(122, 21);
            this.overwrite.Name = "overwrite";
            this.overwrite.Size = new System.Drawing.Size(70, 17);
            this.overwrite.TabIndex = 8;
            this.overwrite.Text = "Overwrite";
            this.overwrite.UseVisualStyleBackColor = true;
            // 
            // major
            // 
            this.major.AutoSize = true;
            this.major.Location = new System.Drawing.Point(65, 21);
            this.major.Name = "major";
            this.major.Size = new System.Drawing.Size(51, 17);
            this.major.TabIndex = 7;
            this.major.Text = "Major";
            this.major.UseVisualStyleBackColor = true;
            // 
            // minor
            // 
            this.minor.AccessibleRole = System.Windows.Forms.AccessibleRole.RadioButton;
            this.minor.AutoSize = true;
            this.minor.Checked = true;
            this.minor.Location = new System.Drawing.Point(8, 21);
            this.minor.Name = "minor";
            this.minor.Size = new System.Drawing.Size(51, 17);
            this.minor.TabIndex = 6;
            this.minor.TabStop = true;
            this.minor.Text = "Minor";
            this.minor.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(288, 197);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 7;
            this.button1.Text = "Continue";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(207, 197);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 8;
            this.button2.Text = "Cancel";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // CommentsWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(375, 231);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comments_text);
            this.Name = "CommentsWindow";
            this.Text = "Check In Details:";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox comments_text;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton overwrite;
        private System.Windows.Forms.RadioButton major;
        private System.Windows.Forms.RadioButton minor;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}