﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Net;
using System.IO;
using Microsoft.SharePoint.Client;
using Ionic.Zip;
using ClaimsAuth;

namespace SPBackupTool
{

    public partial class FileUtility : System.Windows.Forms.Form
    {

        public CookieCollection authCookie;
        public SPWebs.Webs webService = new SPWebs.Webs();
        public SPLists.Lists listService = new SPLists.Lists();
        public SPCopy.Copy copyService = new SPCopy.Copy();

        public FileUtility()
        {
            InitializeComponent();
        }

        private void connect_Click(object sender, EventArgs e)
        {
            // get the cookie collection
            authCookie = ClaimClientContext.GetAuthenticatedCookies(sharepointsite.Text, 525, 525);
            
            webService.PreAuthenticate = true;
            webService.CookieContainer = new CookieContainer();
            webService.CookieContainer.Add(authCookie);

            XmlNode sites = null;
            
            try
            {
                sites = webService.GetAllSubWebCollection();

                // Save the results to an XML file for debugging:
                //XmlDocument doc = new XmlDocument();
                //doc.InnerXml = sites.OuterXml;
                //XmlTextWriter writer = new XmlTextWriter("D:/Desktop/test.xml", null);
                //writer.Formatting = Formatting.Indented;
                //doc.Save(writer);
                //return;

                foreach (XmlNode site in sites.ChildNodes)
                {
                    subsites.Nodes.Add(site.Attributes["Url"].Value);
                }
            }
            catch (Exception err)
            {
                MessageBox.Show("Connection Error: " + err.Message);
            }
        }

        public class fileEntry
        {
            private string _fn; // filename
            private string _lm; // last modified
            private string _url; // url of file
            private uint _fs; // filesize
            private string _pf; // parent folder
            private bool _co; // checked out
            private string _ot; // checked out by
            private string _vr; // version number
            private bool _pb; // published?

            public string fileName { get { return _fn; } set { _fn = value; } }
            public string lastModified { get { return _lm; } set { _lm = value; } }
            public string url { get { return _url; } set { _url = value; } }
            public uint fileSize { get { return _fs; } set { _fs = value; } }
            public string parentFolder { get { return _pf; } set { _pf = value; } }
            public bool checkedOut { get { return _co; } set { _co = value; } }
            public string outTo { get { return _ot; } set { _ot = value; } }
            public string versionNumber { get { return _vr; } set { _vr = value; } }
            public bool published { get { return _pb; } set { _pb = value; } }

            public fileEntry(string fn, string lm, string pf = "", string url = "", uint fs = 0, bool co = false, string ot ="", string vr = "0.1", bool pb = false)
            {
                _fn = fn;
                _lm = lm;
                _url = url;
                _fs = fs;
                _pf = pf;
                _co = co;
                _ot = ot;
                _vr = vr;
                _pb = pb;
            }
        }

        private List<fileEntry> files = new List<fileEntry>();

        private void subsites_AfterSelect(object sender, TreeViewEventArgs e)
        {
            getList(e.Node.Text);
            toolRefresh.Enabled = true;
        }

        private void getList(string siteName, bool all = false) {

            statusBarText.Text = "Working on: " + siteName;
            // Setup List and Copy Services:
            listService.Url = @siteName + @"/_vti_bin/lists.asmx";
            listService.PreAuthenticate = true;
            listService.CookieContainer = new CookieContainer();
            listService.CookieContainer.Add(authCookie);

            // First get a list of all the... well... lists:
            XmlNode lists = listService.GetListCollection();

            // Reset the Listview
            if (!all)
            {
                documentList.Items.Clear();
                documentList.Groups.Clear();
            }

            // Iterate through each one...
            foreach (XmlNode list in lists.ChildNodes)
            {
                // Store the list's name in a variable for easy use:
                string listTitle = list.Attributes["Title"].Value;
                // Set up the new Group Name:
                ListViewGroup groupName = new ListViewGroup(listTitle, HorizontalAlignment.Left);
                documentList.Groups.Add(groupName);
                // Determine the list type:
                string listType = list.Attributes["ServerTemplate"].Value;
                // Now we need to get a list of the files that are stored:
                if (listTitle == "Master Page Gallery" || listType != "101")
                {
                    // We don't want to backup anything other than document lists:
                    continue;
                }

                // Determine the IDs for the listName and the viewName:
                XmlNode listIDs = listService.GetListAndView(listTitle, null);
                string listName = listIDs.FirstChild.Attributes["Name"].Value;
                string viewName = listIDs.LastChild.Attributes["Name"].Value;
                // Set the row limit sufficiently high"
                string rowLimit = "100000000";

                // Add a new group to the document list:
                //ListViewGroup[] listGroups = { new ListViewGroup(listName, HorizontalAlignment.Left) };
                //documentList.Groups.AddRange(listGroups);

                // Set up the query information:
                XmlDocument xmlDoc = new XmlDocument();
                XmlElement query = xmlDoc.CreateElement("Query");
                XmlElement viewFields = xmlDoc.CreateElement("ViewFields");
                XmlElement queryOptions = xmlDoc.CreateElement("QueryOptions");

                // Specify the order of the return:
                if (all)
                {
                    query.InnerXml = "<Where><Geq><FieldRef Name=\"CheckoutUser\" LookupId=\"TRUE\"/><Value Type=\"Integer\">0</Value></Geq></Where>";
                }
                else
                {
                    query.InnerXml = "<Where><Eq><FieldRef Name=\"CheckoutUser\" LookupId=\"TRUE\"/><Value Type=\"Integer\"><UserID Type=\"Integer\" /></Value></Eq></Where><OrderBy><FieldRef Name='EncodedAbsUrl' Ascending='True' /></OrderBy>";
                }
                //query.InnerXml = "<OrderBy><FieldRef Name='EncodedAbsUrl' Ascending='True' /></OrderBy>";
                // Specify any additional fields to return:
                viewFields.InnerXml = "<FieldRef Name='BaseName' /><FieldRef Name='_ModerationStatus' /><FieldRef Name='CheckoutUser' /><FieldRef Name='_UIVersionString' /><FieldRef Name='FileSizeDisplay' /><FieldRef Name='Modified' /><FieldRef Name='EncodedAbsUrl' />";
                // Specify any options with regards to the query:
                queryOptions.InnerXml = "<ViewAttributes Scope='Recursive' IncludeRootFolder='False'/>";

                // Get the list of files:
                XmlNode fileItems = listService.GetListItems(listName, viewName, query, viewFields, rowLimit, queryOptions, null);

                // Save the results to an XML file for debugging:
                //XmlDocument doc = new XmlDocument();
                //doc.InnerXml = fileItems.OuterXml;
                //XmlTextWriter writer = new XmlTextWriter("D:/Desktop/test.xml", null);
                //writer.Formatting = Formatting.Indented;
                //doc.Save(writer);
                //break;

                foreach (XmlNode file in fileItems)
                {
                    if (file.Name == "rs:data")
                    {
                        for (int i = 0; i < file.ChildNodes.Count; i++)
                        {
                            if (file.ChildNodes[i].Name == "z:row")
                            {
                                // Get and format the attributes:
                                string filename = file.ChildNodes[i].Attributes["ows_FileLeafRef"].Value;
                                filename = filename.Replace(file.ChildNodes[i].Attributes["ows_ProgId"].Value, "");

                                string modified = file.ChildNodes[i].Attributes["ows_Modified"].Value;
                                modified = modified.Replace(file.ChildNodes[i].Attributes["ows_ProgId"].Value, "");

                                string checkoutUser = "";
                                try
                                {
                                    checkoutUser = file.ChildNodes[i].Attributes["ows_CheckoutUser"].Value;
                                    int loc = checkoutUser.IndexOf("#");
                                    checkoutUser = checkoutUser.Remove(0, loc + 1);
                                }
                                catch
                                {
                                    checkoutUser = "";
                                }

                                string version = file.ChildNodes[i].Attributes["ows__UIVersionString"].Value;
                                version = version.Replace(file.ChildNodes[i].Attributes["ows_ProgId"].Value, "");

                                string status = file.ChildNodes[i].Attributes["ows__ModerationStatus"].Value;
                                status = status.Replace(file.ChildNodes[i].Attributes["ows_ProgId"].Value, "");

                                switch (status)
                                {
                                    case "0":
                                        status = "Approved";
                                        break;
                                    case "1":
                                        status = "Rejected";
                                        break;
                                    case "2":
                                        status = "Pending";
                                        break;
                                    case "3":
                                        status = "Draft";
                                        break;
                                    case "4":
                                        status = "Scheduled Approval";
                                        break;
                                }

                                string fileUrl = file.ChildNodes[i].Attributes["ows_EncodedAbsUrl"].Value;

                                // Build the list item:
                                ListViewItem lvi = new ListViewItem(filename, 0, groupName);
                                lvi.SubItems.Add(modified);
                                lvi.SubItems.Add(status);
                                lvi.SubItems.Add(checkoutUser);
                                lvi.SubItems.Add(version);
                                lvi.SubItems.Add(fileUrl);

                                // Append the entry to the document list:
                                documentList.Items.Add(lvi);
                            }
                        }
                    }
                }
            }

            statusBarText.Text += "... Done";
        }

        private void toolRefresh_Click(object sender, EventArgs e)
        {
            getList(subsites.SelectedNode.Text);
        }

        private void documentList_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if (documentList.CheckedIndices.Count > 0)
            {
                toolCheckin.Enabled = true;
            }
            else
            {
                toolCheckin.Enabled = false;
            }
        }

        private void toolCheckin_Click(object sender, EventArgs e)
        {
            statusBarText.Text = "Checking documents in...";
            // Get the mass comment and checkin type from the user:
            using (CommentsWindow commentswindow = new CommentsWindow())
            {
                DialogResult result = commentswindow.ShowDialog();
                if (result == DialogResult.OK)
                {
                    string comment = commentswindow.Items[0].ToString();
                    string type = commentswindow.Items[1].ToString();

                    // Grab a list of all the checked file indices:
                    ListView.CheckedIndexCollection indices = documentList.CheckedIndices;

                    foreach (int index in indices)
                    {
                        // Get the file_url from the listview based on the index:
                        string file_url = documentList.Items[index].SubItems[documentList.Columns.Count - 1].Text.ToString();
                        // Check the file in:
                        listService.CheckInFile(file_url, comment, type);
                    }
                    statusBarText.Text += "Done";

                    // Initiate a "refresh":
                    toolRefresh.PerformClick();
                }
            }
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            BackupUtility bkp = new BackupUtility();
            bkp.Show();
        }

        private void uploadTool_Click(object sender, EventArgs e)
        {
            uploadUtility upl = new uploadUtility();
            upl.Show();
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            statusBarText.Text = "Generating Report...";
            // get the cookie collection
            authCookie = ClaimClientContext.GetAuthenticatedCookies(sharepointsite.Text, 525, 525);

            webService.PreAuthenticate = true;
            webService.CookieContainer = new CookieContainer();
            webService.CookieContainer.Add(authCookie);

            XmlNode sites = null;

            // Save the results to an XML file for debugging:
            XmlDocument doc = new XmlDocument();
            XmlNode sites_node = doc.CreateNode(XmlNodeType.Element, "Sites", null);
            sites = webService.GetAllSubWebCollection();
            XmlNode site_node = doc.CreateNode(XmlNodeType.Element, "Site", null);
            XmlNode lists_node = doc.CreateNode(XmlNodeType.Element, "List", null);
            XmlNode files_node = doc.CreateNode(XmlNodeType.Element, "Files", null);

            foreach (XmlNode site in sites.ChildNodes)
            {
                bool sFlag = true;
                string siteName = site.Attributes["Url"].Value;

                // Setup List and Copy Services:
                listService.Url = @siteName + @"/_vti_bin/lists.asmx";
                listService.PreAuthenticate = true;
                listService.CookieContainer = new CookieContainer();
                listService.CookieContainer.Add(authCookie);

                // First get a list of all the... well... lists:
                XmlNode lists = listService.GetListCollection();
                    
                // Iterate through each one...
                foreach (XmlNode list in lists.ChildNodes)
                {
                    bool lFlag = true;
                    // Store the list's name in a variable for easy use:
                    string listTitle = list.Attributes["Title"].Value;



                    // Determine the list type:
                    string listType = list.Attributes["ServerTemplate"].Value;
                    // Now we need to get a list of the files that are stored:
                    if (listTitle == "Master Page Gallery" || listType != "101" || listTitle == "Site Assets")
                    {
                        // We don't want to backup anything other than document lists:
                        continue;
                    }


                    // Determine the IDs for the listName and the viewName:
                    XmlNode listIDs = listService.GetListAndView(listTitle, null);
                    string listName = listIDs.FirstChild.Attributes["Name"].Value;
                    string viewName = listIDs.LastChild.Attributes["Name"].Value;
                    // Set the row limit sufficiently high"
                    string rowLimit = "100000000";

                    // Set up the query information:
                    XmlDocument xmlDoc = new XmlDocument();
                    XmlElement query = xmlDoc.CreateElement("Query");
                    XmlElement viewFields = xmlDoc.CreateElement("ViewFields");
                    XmlElement queryOptions = xmlDoc.CreateElement("QueryOptions");

                    // Specify the order of the return:
                    query.InnerXml = "<Where><Geq><FieldRef Name=\"CheckoutUser\" LookupId=\"TRUE\"/><Value Type=\"Integer\">0</Value></Geq></Where>";
                    viewFields.InnerXml = "<FieldRef Name='BaseName' /><FieldRef Name='_ModerationStatus' /><FieldRef Name='CheckoutUser' /><FieldRef Name='_UIVersionString' /><FieldRef Name='FileSizeDisplay' /><FieldRef Name='Modified' /><FieldRef Name='EncodedAbsUrl' />";
                    queryOptions.InnerXml = "<ViewAttributes Scope='Recursive' IncludeRootFolder='False'/>";

                    // Get the list of files:
                    XmlNode fileItems = listService.GetListItems(listName, viewName, query, viewFields, rowLimit, queryOptions, null);

                    foreach (XmlNode file in fileItems)
                    {
                        if (file.Name == "rs:data")
                        {
                            for (int i = 0; i < file.ChildNodes.Count; i++)
                            {
                                if (file.ChildNodes[i].Name == "z:row")
                                {
                                    // Build Nodes:
                                    if (sFlag)
                                    {
                                        site_node = doc.CreateNode(XmlNodeType.Element, "Site", null);
                                        XmlNode title = doc.CreateElement("title");
                                        title.InnerText = siteName;
                                        site_node.AppendChild(title);
                                        sFlag = false;
                                    }
                                    if (lFlag)
                                    {
                                        lists_node = doc.CreateNode(XmlNodeType.Element, "List", null);
                                        files_node = doc.CreateNode(XmlNodeType.Element, "Files", null);
                                        XmlNode lTitle = doc.CreateElement("ListTitle");
                                        lTitle.InnerText = listTitle;
                                        lists_node.AppendChild(lTitle);
                                        lFlag = false;
                                    }


                                    // Get and format the attributes:
                                    string filename = file.ChildNodes[i].Attributes["ows_FileLeafRef"].Value;
                                    filename = filename.Replace(file.ChildNodes[i].Attributes["ows_ProgId"].Value, "");

                                    string modified = file.ChildNodes[i].Attributes["ows_Modified"].Value;
                                    modified = modified.Replace(file.ChildNodes[i].Attributes["ows_ProgId"].Value, "");

                                    string checkoutUser = "";
                                    try
                                    {
                                        checkoutUser = file.ChildNodes[i].Attributes["ows_CheckoutUser"].Value;
                                        int loc = checkoutUser.IndexOf("#");
                                        checkoutUser = checkoutUser.Remove(0, loc + 1);
                                    }
                                    catch
                                    {
                                        checkoutUser = "";
                                    }

                                    string version = file.ChildNodes[i].Attributes["ows__UIVersionString"].Value;
                                    version = version.Replace(file.ChildNodes[i].Attributes["ows_ProgId"].Value, "");

                                    string status = file.ChildNodes[i].Attributes["ows__ModerationStatus"].Value;
                                    status = status.Replace(file.ChildNodes[i].Attributes["ows_ProgId"].Value, "");

                                    switch (status)
                                    {
                                        case "0":
                                            status = "Approved";
                                            break;
                                        case "1":
                                            status = "Rejected";
                                            break;
                                        case "2":
                                            status = "Pending";
                                            break;
                                        case "3":
                                            status = "Draft";
                                            break;
                                        case "4":
                                            status = "Scheduled Approval";
                                            break;
                                    }

                                    string fileUrl = file.ChildNodes[i].Attributes["ows_EncodedAbsUrl"].Value;

                                    // Build the list item:
                                    XmlNode file_node = doc.CreateNode(XmlNodeType.Element, "File", null);
                                    XmlNode f = doc.CreateElement("Filename");
                                    XmlNode m = doc.CreateElement("Modified");
                                    XmlNode s = doc.CreateElement("Status");
                                    XmlNode c = doc.CreateElement("CheckedOut");

                                    f.InnerText = filename;
                                    m.InnerText = modified;
                                    s.InnerText = status;
                                    c.InnerText = checkoutUser;

                                    file_node.AppendChild(f);
                                    file_node.AppendChild(m);
                                    file_node.AppendChild(s);
                                    file_node.AppendChild(c);

                                    files_node.AppendChild(file_node);
                                }
                            }
                        }
                    }
                    lists_node.AppendChild(files_node);
                    site_node.AppendChild(lists_node);
                    lFlag = true;
                }
                sites_node.AppendChild(site_node);
                sFlag = true;
            }
            doc.InnerXml = sites_node.OuterXml;
            XmlTextWriter writer = new XmlTextWriter("D:/Desktop/test.xml", null);
            writer.Formatting = Formatting.Indented;
            doc.Save(writer);

            statusBarText.Text += "Done";
        }
    }
}
