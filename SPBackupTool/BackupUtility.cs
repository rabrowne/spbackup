﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Net;
using System.IO;
using Microsoft.SharePoint.Client;
using Ionic.Zip;
using ClaimsAuth;


namespace SPBackupTool
{
    public partial class BackupUtility : System.Windows.Forms.Form
    {

        public CookieCollection authCookie;
        public SPWebs.Webs webService = new SPWebs.Webs();
        public SPLists.Lists listService = new SPLists.Lists();
        public SPCopy.Copy copyService = new SPCopy.Copy();

        public BackupUtility()
        {
            InitializeComponent();
        }

        private void connectButton_Click(object sender, EventArgs e)
        {
            // Start debug
            statusBox.AppendText("Connecting...\n");
            // get the cookie collection
            authCookie = ClaimClientContext.GetAuthenticatedCookies(targetUrl.Text, 525, 525);

            statusBox.AppendText("\tCookie grabbed.\n");

            webService.Url = targetUrl.Text + "/_vti_bin/webs.asmx";
            webService.PreAuthenticate = true;
            webService.CookieContainer = new CookieContainer();
            webService.CookieContainer.Add(authCookie);

            XmlNode sites = null;

            try
            {
                sites = webService.GetAllSubWebCollection();

                statusBox.AppendText("Sites:\n");
                foreach (XmlNode site in sites.ChildNodes)
                {
                    sitesList.Items.Add(site.Attributes["Url"].Value);
                    statusBox.AppendText("\t" + site.Attributes["Url"].Value + "\n");
                }

                statusBox.AppendText("Disconnected.\n");

                browseButton.Enabled = true;
                saveLocation.Enabled = true;
                sitesList.Enabled = true;
                backupButton.Enabled = true;
            }
            catch (Exception err)
            {
                statusBox.AppendText("Connection Failed: " + err.Message + "\n");
            }
        }

        private List<fileEntry> existingFiles = new List<fileEntry>();
        private List<fileEntry> filesToBackup = new List<fileEntry>();
        private string tempDirectory;
        private string saveDirectory;
        private string zipFileName;
        private ZipFile zip;
        private uint backupSize = 0;

        public class fileEntry
        {
            private string _fn; // filename
            private string _lm; // last modified
            private string _url; // url of file
            private uint _fs; // filesize
            private string _pf; // parent folder

            public string fileName { get { return _fn; } set { _fn = value; } }
            public string lastModified { get { return _lm; } set { _lm = value; } }
            public string url { get { return _url; } set { _url = value; } }
            public uint fileSize { get { return _fs; } set { _fs = value; } }
            public string parentFolder { get { return _pf; } set { _pf = value; } }

            public fileEntry(string fn, string lm, string pf = "", string url = "", uint fs = 0)
            {
                _fn = fn;
                _lm = lm;
                _url = url;
                _fs = fs;
                _pf = pf;
            }
        }

        private void getExisitingFiles(string siteName)
        {
            // Check file structure for exisiting files for each of the "sub_sites".
            string saveLoc = saveDirectory + "\\" + siteName;
            zipFileName = saveLoc + "\\" + siteName + ".zip";
            tempDirectory = saveLoc + "\\temp";

            if (System.IO.Directory.Exists(saveLoc))
            {
                // Folder already exists
                statusBox.AppendText(saveLoc + " found; reading contents:\n");
                var files = System.IO.Directory.GetFiles(saveLoc, "*.*", System.IO.SearchOption.AllDirectories);
                foreach (var file in files)
                {
                    string filename = file;
                    int loc = filename.LastIndexOf("\\");
                    filename = filename.Remove(0, (loc + 1));
                    var fileAttr = System.IO.File.GetAttributes(file);
                    var i = 0;
                    // existingFiles.Add(new fileEntry(filename, fileAttr["LastModified"].ToString("yyyy-MM-dd HH:mm:ss"), "", "", Convert.ToUInt32(fileAttr["FileSize"])));
                }
            }
            else
            {
                System.IO.Directory.CreateDirectory(saveLoc);
                // Zip file does not exist already
                //statusBox.AppendText(zipFileName + " not found; creating new zip:\n");
                //using (zip = new ZipFile())
                //{
                //    zip.Save(zipFileName);
                //}
                //statusBox.AppendText("Zip Created\n");
            }
            statusBox.AppendText("\t" + existingFiles.Count + " Files Existing in Zip\n");
        }

        private void getListFiles()
        {
            // Check the list and get a list of files to download into the backup zip
            try
            {
                // First get a list of all the... well... lists:
                XmlNode lists = listService.GetListCollection();

                statusBox.AppendText("Lists:\n");
                // Iterate through each one...
                foreach (XmlNode list in lists.ChildNodes)
                {
                    // Store the list's name in a variable for easy use:
                    string listTitle = list.Attributes["Title"].Value;
                    // Determine the list type:
                    string listType = list.Attributes["ServerTemplate"].Value;
                    // Now we need to get a list of the files that are stored:
                    if (listTitle == "Master Page Gallery" || listType != "101" )
                    {
                        // We don't want to backup anything other than document lists:
                        continue;
                    }
                    statusBox.AppendText("\t" + listTitle + ":\n");

                    // Determine the IDs for the listName and the viewName:
                    XmlNode listIDs = listService.GetListAndView(listTitle, null);
                    string listName = listIDs.FirstChild.Attributes["Name"].Value;
                    string viewName = listIDs.LastChild.Attributes["Name"].Value;
                    // Set the row limit sufficiently high"
                    string rowLimit = "100000000";

                    // Set up the query information:
                    XmlDocument xmlDoc = new XmlDocument();
                    XmlElement query = xmlDoc.CreateElement("Query");
                    XmlElement viewFields = xmlDoc.CreateElement("ViewFields");
                    XmlElement queryOptions = xmlDoc.CreateElement("QueryOptions");

                    // Specify the order of the return:
                    query.InnerXml = "<OrderBy><FieldRef Name='EncodedAbsUrl' Ascending='True' /></OrderBy>";
                    // Specify any additional fields to return:
                    viewFields.InnerXml = "<FieldRef Name='BaseName' /><FieldRef Name='FileSizeDisplay' /><FieldRef Name='Modified' /><FieldRef Name='EncodedAbsUrl' />";
                    // Specify any options with regards to the query:
                    queryOptions.InnerXml = "<ViewAttributes Scope='Recursive' IncludeRootFolder='False'/>";

                    // Get the list of files:
                    XmlNode fileItems = listService.GetListItems(listName, viewName, query, viewFields, rowLimit, queryOptions, null);

                    // Save the results to an XML file for debugging:
                    //XmlDocument doc = new XmlDocument();
                    //doc.InnerXml = fileItems.OuterXml;
                    //XmlTextWriter writer = new XmlTextWriter("C:/Users/rbrowne/Desktop/test.xml", null);
                    //writer.Formatting = Formatting.Indented;
                    //doc.Save(writer);

                    // Iterate through each file in the list to determine if it needs to be "backed" up:
                    foreach (XmlNode item in fileItems)
                    {
                        if (item.Name == "rs:data")
                        {
                            statusBox.AppendText("\t" + item.Attributes["ItemCount"].Value + " Files in List\n");
                            for (int i = 0; i < item.ChildNodes.Count; i++)
                            {
                                if (item.ChildNodes[i].Name == "z:row")
                                {
                                    // Put the XML of a file into a variable for use:
                                    XmlElement file = (XmlElement)item.ChildNodes[i];
                                    // Determine the filename:
                                    string fn = file.Attributes["ows_FileLeafRef"].Value;
                                    // Remove annoying number at beginning of name:
                                    fn = fn.Replace(file.Attributes["ows_ProgId"].Value, "");
                                    // Get the lastmodified date of the file:
                                    string lm = file.Attributes["ows_Last_x0020_Modified"].Value;
                                    // Remove annoying number at beginning:
                                    lm = lm.Replace(file.Attributes["ows_ProgId"].Value, "");
                                    // Get the url of the file:
                                    string url = file.Attributes["ows_EncodedAbsUrl"].Value;
                                    // Get the filesize of the file:
                                    uint fileSize = Convert.ToUInt32(file.Attributes["ows_FileSizeDisplay"].Value);
                                    // Get the folder location of the file:
                                    string folder = file.Attributes["ows_FileRef"].Value;
                                    // Remove the annoying number and site name from the start:
                                    int loc = folder.IndexOf("/");
                                    folder = folder.Remove(0, (loc + 1));
                                    // Remove the filename from the end of the folder:
                                    loc = folder.LastIndexOf("/");
                                    folder = folder.Remove(loc);

                                    // Do we have any existingFiles:
                                    if (existingFiles.Count > 0)
                                    {
                                        // Yes, check current files for entry:
                                        try
                                        {
                                            fileEntry found = existingFiles.FirstOrDefault(o => (o.fileName == fn) && (Convert.ToDateTime(o.lastModified) > Convert.ToDateTime(lm)) && o.fileSize == fileSize);
                                            if (found == null)
                                            {
                                                // We have a file that needs backing up:
                                                filesToBackup.Add(new fileEntry(fn, lm, folder, url, fileSize));
                                                // Add the file size to the counter:
                                                backupSize += fileSize;
                                            }
                                        }
                                        catch (Exception ane)
                                        {
                                            statusBox.AppendText("\t" + ane.Message);
                                        }
                                    }
                                    else
                                    {
                                        // No, so we want to backup everything:
                                        filesToBackup.Add(new fileEntry(fn, lm, folder, url, fileSize));
                                        // Add the file size to the counter:
                                        backupSize += fileSize;
                                    }
                                }
                            }
                        }
                    }
                    // Print out some details:
                    statusBox.AppendText("\t" + filesToBackup.Count + " Files Totalling: " + convertFileSize(backupSize) + " need backing up\n");
                    updateProgress((int)backupSize, true);
                }
            }
            catch (Exception err)
            {
                statusBox.AppendText("Connection Failed: " + err.Message + "\n");
            }
        }

        private void backupFiles()
        {
            
            // Perform the backup of all the files:
            //using (zip = ZipFile.Read(zipFileName))
            //{
            //    zip.CompressionLevel = Ionic.Zlib.CompressionLevel.BestCompression;

            foreach (fileEntry file in filesToBackup)
            {
                // Get the file info:
                string fileName = file.fileName;
                string lastModified = file.lastModified;
                string folder = file.parentFolder;
                string url = file.url;
                uint fileSize = file.fileSize;

                statusBox.AppendText("\t Backing up: " + fileName + "\n");
                
                // Set up the variables:
                SPCopy.FieldInformation fileInfo = new SPCopy.FieldInformation();
                SPCopy.FieldInformation[] fileInfoArray = { fileInfo };
                byte[] byteArray;

                folder = folder.Substring(folder.IndexOf("/")+1);
                folder = folder.Replace("/", "\\");
                folder = saveDirectory + "\\" + folder;

                // Perform the "copy" from the server:
                uint getUint = copyService.GetItem(url, out fileInfoArray, out byteArray);
                // Store the data in a temporary location:
                CopyFileToDirectory(byteArray, fileName, folder);
                // Add the file to the zip file:
                // zip.UpdateItem(tempPath, folder);
                // Save the zip file incase the next file fails:
                // zip.Save(zipFileName);
                // Delete Temporary File:
                // CleanUpFileSystem(fileName);
                // Update the progres bar:
                updateProgress(Convert.ToInt32(fileSize));
            }
            //}
        }

        private DateTime tstart = DateTime.Now;
        private TimeSpan diffTime;
        private int dataRemaining = 0;
        private int dataTotal = 0;
        public TimeSpan eta;
        private double downloadSpeed = 0.0;

        private void updateProgress(int size, bool reset = false)
        {
            // Calculate the difference in time since the start:
            diffTime = DateTime.Now - tstart;
            if (reset)
            {
                // We are starting a new backup process so lets reset:
                statusProgressBar.Maximum = size;
                dataTotal = size;
                dataRemaining = size;
                statusProgressBar.Value = 0;
                eta = TimeSpan.FromSeconds(0);
            }
            else
            {
                // We're in the process of backing things up so lets update the progress/eta based on what we've done thus far:
                statusProgressBar.Value += size;
                dataRemaining -= size;
                eta = TimeSpan.FromMilliseconds(dataRemaining / ((dataTotal - dataRemaining) / diffTime.TotalMilliseconds));
                downloadSpeed = (dataRemaining/(1024*1024)) / eta.TotalSeconds;
            }
            // Set the status bar texts to suit:
            statusDataRemaining.Text = string.Format("{0}", convertFileSize(Convert.ToUInt32(dataRemaining)));
            statusDownloadSpeed.Text = string.Format("{0:0.00} Mbps", downloadSpeed);
            statusETA.Text = string.Format("{0:00}:{1:00}:{2:00}", (int)eta.TotalHours, eta.Minutes, eta.Seconds);
            // Refresh the status bar:
            statusDataRemaining.Update();
            statusDownloadSpeed.Update();
            statusETA.Update();
        }
        
        private void saveLocation_MouseClick(object sender, MouseEventArgs e)
        {
            browseButton.PerformClick();
        }

        private void browseButton_Click(object sender, EventArgs e)
        {
            saveLocationDialog.ShowDialog();
            saveLocation.Text = saveLocationDialog.SelectedPath;
        }

        private bool stopInterupt = true;

        private void backupButton_Click(object sender, EventArgs e)
        {
            
            while (stopInterupt) {
                statusBox.AppendText("---\nBackup Started!\n");
                // Setup directory folders:
                saveDirectory = saveLocation.Text;

                // Get a list of all the sites selected for backup:
                var sites = sitesList.CheckedItems;
                string siteName;
                // Iterate through each site and backup as needed:
                foreach (ListViewItem site in sites)
                {
                    siteName = site.Text;
                    statusBox.AppendText("Beginning Backup of: " + siteName + "\n");

                    // Ensure that the existingFiles list is empty:
                    existingFiles = new List<fileEntry>();
                    // Ensure that the filesToBackup list is empty:
                    filesToBackup = new List<fileEntry>();
                    // Reset backupSize counter:
                    backupSize = 0;

                    // Setup List and Copy Services:
                    listService.Url = @siteName + @"/_vti_bin/lists.asmx";
                    listService.PreAuthenticate = true;
                    listService.CookieContainer = new CookieContainer();
                    listService.CookieContainer.Add(authCookie);

                    copyService.Url = @siteName + @"/_vti_bin/copy.asmx";
                    copyService.PreAuthenticate = true;
                    copyService.CookieContainer = new CookieContainer();
                    copyService.CookieContainer.Add(authCookie);

                    // Set the siteName that we're working on now:
                    int loc = siteName.LastIndexOf("/");
                    siteName = siteName.Remove(0, loc + 1);
                    // Now get the details of all the files we have presently backed up:
                    getExisitingFiles(siteName);
                    // Now get a list of all the files that need to be backed up:
                    getListFiles();
                    // Now that we have a list of all the files that we need to backup, lets start backing them up:
                    backupFiles();
                }
            }
            // All done!
            statusBox.AppendText("Backup Complete!\n");
        }

        public Array listIgnore;

        private string convertFileSize(uint fileSize)
        {
            string append = "B";
            if (fileSize > 1024)
            {
                fileSize = fileSize / 1024;
                append = "kB";
            }
            if (fileSize > 1024)
            {
                fileSize = fileSize / 1024;
                append = "MB";
            }
            if (fileSize > 1024)
            {
                fileSize = fileSize / 1024;
                append = "GB";
            }

            return (fileSize.ToString() + append);
        }

        private string CopyFileToTempDirectory(byte[] buffer, string fileName)
        { 
            try
            {
                // Create a temporary directory:
                Directory.CreateDirectory(tempDirectory);
                
                // Write a new file to the temp directory:
                var strfile = tempDirectory + "\\" + fileName;
                System.IO.File.WriteAllBytes(strfile, buffer);
            
                return strfile;
            }
            catch (Exception e)
            {
                return "";
            }
        }

        private string CopyFileToDirectory(byte[] buffer, string fileName, string folderName)
        {
            try
            {
                // Create a temporary directory:
                Directory.CreateDirectory(folderName);

                // Write a new file to the temp directory:
                var strfile = folderName + "\\" + fileName;
                System.IO.File.WriteAllBytes(strfile, buffer);

                return strfile;
            }
            catch (Exception e)
            {
                return "";
            }
        }

        private void CleanUpFileSystem(string fileName)
        {
            // Delete the file from the system:
            System.IO.File.Delete(string.Format("{0}\\{1}", tempDirectory, fileName));
            // Delete the temp directory from the system:
            Directory.Delete(tempDirectory + "\\", true);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //connectButton.PerformClick();
        }

        private void checkAll_CheckedChanged(object sender, EventArgs e)
        {
            foreach (ListViewItem item in sitesList.Items)
            {
                item.Checked = checkAll.Checked;
            }
        }

        private void stopButton_Click(object sender, EventArgs e)
        {
            backupButton.Visible = true;
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.Form aboutDialog = new AboutBox1();

            aboutDialog.Show();
        }

       
    }
}
