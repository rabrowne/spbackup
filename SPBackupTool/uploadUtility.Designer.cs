﻿namespace SPBackupTool
{
    partial class uploadUtility
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.browseButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.saveLocation = new System.Windows.Forms.TextBox();
            this.connectButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.targetUrl = new System.Windows.Forms.TextBox();
            this.loadLocationDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.button1 = new System.Windows.Forms.Button();
            this.statusBox = new System.Windows.Forms.TextBox();
            this.templateType = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.overallProgress = new System.Windows.Forms.ProgressBar();
            this.siteProgress = new System.Windows.Forms.ProgressBar();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.workingLabel = new System.Windows.Forms.Label();
            this.overallETA = new System.Windows.Forms.Label();
            this.uploadSpeed = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // browseButton
            // 
            this.browseButton.Enabled = false;
            this.browseButton.Location = new System.Drawing.Point(479, 61);
            this.browseButton.Name = "browseButton";
            this.browseButton.Size = new System.Drawing.Size(75, 23);
            this.browseButton.TabIndex = 18;
            this.browseButton.Text = "Browse";
            this.browseButton.UseVisualStyleBackColor = true;
            this.browseButton.Click += new System.EventHandler(this.browseButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(156, 13);
            this.label2.TabIndex = 17;
            this.label2.Text = "Select Restore Folder Location:";
            // 
            // saveLocation
            // 
            this.saveLocation.Enabled = false;
            this.saveLocation.Location = new System.Drawing.Point(15, 63);
            this.saveLocation.Name = "saveLocation";
            this.saveLocation.Size = new System.Drawing.Size(458, 20);
            this.saveLocation.TabIndex = 16;
            this.saveLocation.Text = "[Browse]";
            // 
            // connectButton
            // 
            this.connectButton.Location = new System.Drawing.Point(479, 23);
            this.connectButton.Name = "connectButton";
            this.connectButton.Size = new System.Drawing.Size(75, 23);
            this.connectButton.TabIndex = 15;
            this.connectButton.Text = "Connect";
            this.connectButton.UseVisualStyleBackColor = true;
            this.connectButton.Click += new System.EventHandler(this.connectButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(203, 13);
            this.label1.TabIndex = 14;
            this.label1.Text = "URL of Sharepoint site to create subsites:";
            // 
            // targetUrl
            // 
            this.targetUrl.Location = new System.Drawing.Point(15, 25);
            this.targetUrl.Name = "targetUrl";
            this.targetUrl.Size = new System.Drawing.Size(458, 20);
            this.targetUrl.TabIndex = 13;
            this.targetUrl.Text = "https://[somesite].sharepoint.com";
            // 
            // button1
            // 
            this.button1.Enabled = false;
            this.button1.Location = new System.Drawing.Point(446, 99);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(108, 23);
            this.button1.TabIndex = 20;
            this.button1.Text = "Begin Upload";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // statusBox
            // 
            this.statusBox.Location = new System.Drawing.Point(15, 234);
            this.statusBox.Multiline = true;
            this.statusBox.Name = "statusBox";
            this.statusBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.statusBox.Size = new System.Drawing.Size(539, 130);
            this.statusBox.TabIndex = 21;
            // 
            // templateType
            // 
            this.templateType.Enabled = false;
            this.templateType.FormattingEnabled = true;
            this.templateType.Items.AddRange(new object[] {
            "Proposal Template",
            "Project Template",
            "Support Template"});
            this.templateType.Location = new System.Drawing.Point(15, 101);
            this.templateType.Name = "templateType";
            this.templateType.Size = new System.Drawing.Size(200, 21);
            this.templateType.TabIndex = 22;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 86);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(114, 13);
            this.label3.TabIndex = 23;
            this.label3.Text = "Select Template Type:";
            // 
            // overallProgress
            // 
            this.overallProgress.Location = new System.Drawing.Point(13, 154);
            this.overallProgress.Name = "overallProgress";
            this.overallProgress.Size = new System.Drawing.Size(541, 23);
            this.overallProgress.TabIndex = 24;
            // 
            // siteProgress
            // 
            this.siteProgress.Location = new System.Drawing.Point(13, 196);
            this.siteProgress.Name = "siteProgress";
            this.siteProgress.Size = new System.Drawing.Size(541, 23);
            this.siteProgress.TabIndex = 25;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 138);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(87, 13);
            this.label4.TabIndex = 26;
            this.label4.Text = "Overall Progress:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 180);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(109, 13);
            this.label5.TabIndex = 27;
            this.label5.Text = "Current Site Progress:";
            // 
            // workingLabel
            // 
            this.workingLabel.AutoSize = true;
            this.workingLabel.Location = new System.Drawing.Point(127, 180);
            this.workingLabel.MaximumSize = new System.Drawing.Size(250, 13);
            this.workingLabel.MinimumSize = new System.Drawing.Size(250, 13);
            this.workingLabel.Name = "workingLabel";
            this.workingLabel.Size = new System.Drawing.Size(250, 13);
            this.workingLabel.TabIndex = 28;
            // 
            // overallETA
            // 
            this.overallETA.AutoSize = true;
            this.overallETA.Location = new System.Drawing.Point(428, 180);
            this.overallETA.MaximumSize = new System.Drawing.Size(60, 13);
            this.overallETA.MinimumSize = new System.Drawing.Size(60, 13);
            this.overallETA.Name = "overallETA";
            this.overallETA.Size = new System.Drawing.Size(60, 13);
            this.overallETA.TabIndex = 29;
            this.overallETA.Text = "00:00:00";
            this.overallETA.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // uploadSpeed
            // 
            this.uploadSpeed.AutoSize = true;
            this.uploadSpeed.Location = new System.Drawing.Point(494, 180);
            this.uploadSpeed.MaximumSize = new System.Drawing.Size(60, 13);
            this.uploadSpeed.MinimumSize = new System.Drawing.Size(60, 13);
            this.uploadSpeed.Name = "uploadSpeed";
            this.uploadSpeed.Size = new System.Drawing.Size(60, 13);
            this.uploadSpeed.TabIndex = 30;
            this.uploadSpeed.Text = "0MBps";
            this.uploadSpeed.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // uploadUtility
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(572, 376);
            this.Controls.Add(this.uploadSpeed);
            this.Controls.Add(this.overallETA);
            this.Controls.Add(this.workingLabel);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.siteProgress);
            this.Controls.Add(this.overallProgress);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.templateType);
            this.Controls.Add(this.statusBox);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.browseButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.saveLocation);
            this.Controls.Add(this.connectButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.targetUrl);
            this.Name = "uploadUtility";
            this.Text = "Upload Utility";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button browseButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox saveLocation;
        private System.Windows.Forms.Button connectButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox targetUrl;
        private System.Windows.Forms.FolderBrowserDialog loadLocationDialog;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox statusBox;
        private System.Windows.Forms.ComboBox templateType;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ProgressBar overallProgress;
        private System.Windows.Forms.ProgressBar siteProgress;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label workingLabel;
        private System.Windows.Forms.Label overallETA;
        private System.Windows.Forms.Label uploadSpeed;
    }
}