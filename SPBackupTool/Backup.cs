﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Net;
using System.Xml;
using System.Windows.Forms;
using Microsoft.SharePoint.Client;
using Ionic.Zip;
using ClaimsAuth;

namespace SPBackupTool
{
    public class Backup
    {
        public class CurrentState
        {
            public uint fileSize;
            public uint listSize;
            public string fileName;
        }

        public class fileEntry
        {
            private string _fn; // filename
            private string _lm; // last modified
            private string _url; // url of file
            private uint _fs; // filesize
            private string _pf; // parent folder

            public string fileName { get { return _fn; } set { _fn = value; } }
            public string lastModified { get { return _lm; } set { _lm = value; } }
            public string url { get { return _url; } set { _url = value; } }
            public uint fileSize { get { return _fs; } set { _fs = value; } }
            public string parentFolder { get { return _pf; } set { _pf = value; } }

            public fileEntry(string fn, string lm, string pf = "", string url = "", uint fs = 0)
            {
                _fn = fn;
                _lm = lm;
                _url = url;
                _fs = fs;
                _pf = pf;
            }
        }

        public XmlNode siteUrls;
        public string savePath;

        private string siteUrl = "";
        private string tempDirectory;
        private string zipFileName;
        private ZipFile zip;
        private int elapsedTime = 20;
        private uint backupSize = 0;

        public CookieCollection authCookie;
        public SPWebs.Webs webService = new SPWebs.Webs();
        public SPLists.Lists listService = new SPLists.Lists();
        public SPCopy.Copy copyService = new SPCopy.Copy();

        private List<fileEntry> existingFiles = new List<fileEntry>();
        private List<fileEntry> filesToBackup = new List<fileEntry>();

        public void PerformBackup(System.ComponentModel.BackgroundWorker worker, System.ComponentModel.DoWorkEventArgs e)
        {

            foreach (XmlNode site in siteUrls)
            {
                siteUrl = site.Attributes["Url"].Value;
                // Setup directory folders:
                tempDirectory = string.Format("{0}\\sp_backup_temp", savePath);
                // Ensure that the existingFiles list is empty:
                existingFiles = new List<fileEntry>();
                // Ensure that the filesToBackup list is empty:
                filesToBackup = new List<fileEntry>();
                
                // Set the siteName that we're working on now:
                int loc = siteUrl.LastIndexOf("/");
                siteUrl = siteUrl.Remove(0, loc + 1);
                // Now get the details of all the files we have presently backed up:
                getExisitingFiles(siteUrl);
                // Now get a list of all the files that need to be backed up:
                getListFiles();
                // Now that we have a list of all the files that we need to backup, lets start backing them up:
                backupFiles(worker, e);
                // Site done!
            }
        }

        private void getExisitingFiles(string siteName)
        {
            // Check file structure for exisiting files for each of the "sub_sites".
            zipFileName = savePath + "\\" + siteName + ".zip";

            if (System.IO.File.Exists(zipFileName))
            {
                // Zip file exists
                using (zip = ZipFile.Read(zipFileName))
                {
                    foreach (ZipEntry entry in zip)
                    {
                        string filename = entry.FileName;
                        int loc = filename.LastIndexOf("/");
                        filename = filename.Remove(0, (loc + 1));
                        existingFiles.Add(new fileEntry(filename, entry.LastModified.ToString("yyyy-MM-dd HH:mm:ss"), "", "", Convert.ToUInt32(entry.UncompressedSize)));
                    }
                }
            }
            else
            {
                // Zip file does not exist already
                using (zip = new ZipFile())
                {
                    zip.Save(zipFileName);
                }
            }
        }

        private void getListFiles()
        {
            // Check the list and get a list of files to download into the backup zip
            try
            {
                // First get a list of all the... well... lists:
                XmlNode lists = listService.GetListCollection();

                // Iterate through each one...
                foreach (XmlNode list in lists.ChildNodes)
                {
                    // Store the list's name in a variable for easy use:
                    string listTitle = list.Attributes["Title"].Value;
                    // Determine the list type:
                    string listType = list.Attributes["ServerTemplate"].Value;
                    // Now we need to get a list of the files that are stored:
                    if (listTitle == "Master Page Gallery" || listType != "101")
                    {
                        // We don't want to backup anything other than document lists:
                        continue;
                    }

                    // Determine the IDs for the listName and the viewName:
                    XmlNode listIDs = listService.GetListAndView(listTitle, null);
                    string listName = listIDs.FirstChild.Attributes["Name"].Value;
                    string viewName = listIDs.LastChild.Attributes["Name"].Value;
                    // Set the row limit sufficiently high"
                    string rowLimit = "100000000";

                    // Set up the query information:
                    XmlDocument xmlDoc = new XmlDocument();
                    XmlElement query = xmlDoc.CreateElement("Query");
                    XmlElement viewFields = xmlDoc.CreateElement("ViewFields");
                    XmlElement queryOptions = xmlDoc.CreateElement("QueryOptions");

                    // Specify the order of the return:
                    query.InnerXml = "<OrderBy><FieldRef Name='EncodedAbsUrl' Ascending='True' /></OrderBy>";
                    // Specify any additional fields to return:
                    viewFields.InnerXml = "<FieldRef Name='BaseName' /><FieldRef Name='FileSizeDisplay' /><FieldRef Name='Modified' /><FieldRef Name='EncodedAbsUrl' />";
                    // Specify any options with regards to the query:
                    queryOptions.InnerXml = "<ViewAttributes Scope='Recursive' IncludeRootFolder='False'/>";

                    // Get the list of files:
                    XmlNode fileItems = listService.GetListItems(listName, viewName, query, viewFields, rowLimit, queryOptions, null);

                    // Save the results to an XML file for debugging:
                    //XmlDocument doc = new XmlDocument();
                    //doc.InnerXml = fileItems.OuterXml;
                    //XmlTextWriter writer = new XmlTextWriter("C:/Users/rbrowne/Desktop/test.xml", null);
                    //writer.Formatting = Formatting.Indented;
                    //doc.Save(writer);

                    // Iterate through each file in the list to determine if it needs to be "backed" up:
                    foreach (XmlNode item in fileItems)
                    {
                        if (item.Name == "rs:data")
                        {
                            for (int i = 0; i < item.ChildNodes.Count; i++)
                            {
                                if (item.ChildNodes[i].Name == "z:row")
                                {
                                    // Put the XML of a file into a variable for use:
                                    XmlElement file = (XmlElement)item.ChildNodes[i];
                                    // Determine the filename:
                                    string fn = file.Attributes["ows_FileLeafRef"].Value;
                                    // Remove annoying number at beginning of name:
                                    fn = fn.Replace(file.Attributes["ows_ProgId"].Value, "");
                                    // Get the lastmodified date of the file:
                                    string lm = file.Attributes["ows_Last_x0020_Modified"].Value;
                                    // Remove annoying number at beginning:
                                    lm = lm.Replace(file.Attributes["ows_ProgId"].Value, "");
                                    // Get the url of the file:
                                    string url = file.Attributes["ows_EncodedAbsUrl"].Value;
                                    // Get the filesize of the file:
                                    uint fileSize = Convert.ToUInt32(file.Attributes["ows_FileSizeDisplay"].Value);
                                    // Get the folder location of the file:
                                    string folder = file.Attributes["ows_FileRef"].Value;
                                    // Remove the annoying number and site name from the start:
                                    int loc = folder.IndexOf("/");
                                    folder = folder.Remove(0, (loc + 1));
                                    // Remove the filename from the end of the folder:
                                    loc = folder.LastIndexOf("/");
                                    folder = folder.Remove(loc);

                                    // Do we have any existingFiles:
                                    if (existingFiles.Count > 0)
                                    {
                                        // Yes, check current files for entry:
                                        try
                                        {
                                            fileEntry found = existingFiles.FirstOrDefault(o => (o.fileName == fn) && (Convert.ToDateTime(o.lastModified) > Convert.ToDateTime(lm)) && o.fileSize == fileSize);
                                            if (found == null)
                                            {
                                                // We have a file that needs backing up:
                                                filesToBackup.Add(new fileEntry(fn, lm, folder, url, fileSize));
                                                // Add the file size to the counter:
                                                backupSize += fileSize;
                                            }
                                        }
                                        catch (Exception e)
                                        {
                                            throw new Exception(e.Message);
                                        }
                                    }
                                    else
                                    {
                                        // No, so we want to backup everything:
                                        filesToBackup.Add(new fileEntry(fn, lm, folder, url, fileSize));
                                        // Add the file size to the counter:
                                        backupSize += fileSize;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception err)
            {
                throw new Exception("Connection Failed: " + err.Message + "\n");
            }
        }

        private void backupFiles(System.ComponentModel.BackgroundWorker worker, System.ComponentModel.DoWorkEventArgs e)
        {
            // Set up variables:
            DateTime lastReportDateTime = DateTime.Now;
            CurrentState state = new CurrentState();
            string fileName = "";
            uint fileSize = 0;
            // Perform the backup of all the files:
            using (zip = ZipFile.Read(zipFileName))
            {
                zip.CompressionLevel = Ionic.Zlib.CompressionLevel.BestCompression;

                foreach (fileEntry file in filesToBackup)
                {
                    if (worker.CancellationPending)
                    {
                        e.Cancel = true;
                        break;
                    }
                    else
                    {

                        // Get the file info:
                        fileName = file.fileName;
                        string lastModified = file.lastModified;
                        string folder = file.parentFolder;
                        string url = file.url;
                        fileSize = file.fileSize;

                        // Set up the variables:
                        SPCopy.FieldInformation fileInfo = new SPCopy.FieldInformation();
                        SPCopy.FieldInformation[] fileInfoArray = { fileInfo };
                        byte[] byteArray;

                        // Perform the "copy" from the server:
                        uint getUint = copyService.GetItem(url, out fileInfoArray, out byteArray);
                        // Store the data in a temporary location:
                        string tempPath = CopyFileToTempDirectory(byteArray, fileName);
                        // Add the file to the zip file:
                        zip.UpdateItem(tempPath, folder);
                        // Save the zip file incase the next file fails:
                        zip.Save(zipFileName);
                        // Delete Temporary File:
                        CleanUpFileSystem(fileName);
                        // Update on the progress:
                        int compare = DateTime.Compare(DateTime.Now, lastReportDateTime.AddMilliseconds(elapsedTime));
                        if (compare > 0)
                        {
                            // It's about time we reported back:
                            state.fileSize = fileSize;
                            state.fileName = fileName;
                            state.listSize = backupSize;
                            worker.ReportProgress(0, state);
                            lastReportDateTime = DateTime.Now;
                        }
                    }
                }

                // Report the final answers.
                state.fileSize = fileSize;
                state.fileName = fileName;
                state.listSize = backupSize;
                worker.ReportProgress(0, state);
            }
        }

        private string CopyFileToTempDirectory(byte[] buffer, string fileName)
        {
            try
            {
                // Create a temporary directory:
                Directory.CreateDirectory(tempDirectory);

                // Write a new file to the temp directory:
                System.IO.File.WriteAllBytes(string.Format("{0}\\{1}", tempDirectory, fileName), buffer);

                string tempFullPath = string.Format("{0}\\{1}", tempDirectory, fileName);
                return tempFullPath;
            }
            catch (Exception e)
            {
                return "";
            }
        }

        private void CleanUpFileSystem(string fileName)
        {
            // Delete the file from the system:
            System.IO.File.Delete(string.Format("{0}\\{1}", tempDirectory, fileName));
            // Delete the temp directory from the system:
            Directory.Delete(tempDirectory + "\\", true);
        }
    }
}
