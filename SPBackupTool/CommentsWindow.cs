﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SPBackupTool
{
    public partial class CommentsWindow : Form
    {
        public CommentsWindow()
        {
            InitializeComponent();
        }

        public object[] Items;

        private void button1_Click(object sender, EventArgs e)
        {
            string comments = comments_text.Text;
            string type = "";
            if (minor.Checked)
            {
                type = "0";
            }
            else if (major.Checked)
            {
                type = "1";
            }
            else
            {
                type = "2";
            }
            // MessageBox.Show(comments + " - Type: " + type);
            Items = new object[] { comments, type };
            DialogResult = DialogResult.OK;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
    }
}
