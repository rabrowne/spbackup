﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Net;
using System.IO;
using Microsoft.SharePoint.Client;
using Ionic.Zip;
using ClaimsAuth;

namespace SPBackupTool
{
    public partial class uploadUtility : System.Windows.Forms.Form
    {
        public uploadUtility()
        {
            InitializeComponent();
        }
        
        public CookieCollection authCookie;
        public SPWebs.Webs webService = new SPWebs.Webs();
        public SPSites.Sites siteService = new SPSites.Sites();
        public SPLists.Lists listService = new SPLists.Lists();
        public SPCopy.Copy copyService = new SPCopy.Copy();
        public SPDws.Dws dwsService = new SPDws.Dws();

        private void connectButton_Click(object sender, EventArgs e)
        {
            // Start debug
            statusBox.AppendText("Connecting...");
            // get the cookie collection
            authCookie = ClaimClientContext.GetAuthenticatedCookies(targetUrl.Text, 525, 525);

            statusBox.AppendText("Done.\n");

            webService.PreAuthenticate = true;
            webService.CookieContainer = new CookieContainer();
            webService.CookieContainer.Add(authCookie);

            siteService.PreAuthenticate = true;
            siteService.CookieContainer = new CookieContainer();
            siteService.CookieContainer.Add(authCookie);

            copyService.PreAuthenticate = true;
            copyService.CookieContainer = new CookieContainer();
            copyService.CookieContainer.Add(authCookie);

            listService.PreAuthenticate = true;
            listService.CookieContainer = new CookieContainer();
            listService.CookieContainer.Add(authCookie);

            dwsService.PreAuthenticate = true;
            dwsService.CookieContainer = new CookieContainer();
            dwsService.CookieContainer.Add(authCookie);

            browseButton.Enabled = true;
            saveLocation.Enabled = true;
            button1.Enabled = true;
            templateType.Enabled = true;
        }

        private void browseButton_Click(object sender, EventArgs e)
        {
            loadLocationDialog.ShowDialog();
            saveLocation.Text = loadLocationDialog.SelectedPath;
        }

        private void getFolders()
        {
            var folderLoc = saveLocation.Text;


        }

        static IEnumerable<string> GetFiles(string path)
        {
            Queue<string> queue = new Queue<string>();
            queue.Enqueue(path);
            while (queue.Count > 0)
            {
                path = queue.Dequeue();
                try
                {
                    foreach (string subDir in Directory.GetDirectories(path))
                    {
                        queue.Enqueue(subDir);
                    }
                }
                catch (Exception ex)
                {
                    Console.Error.WriteLine(ex);
                }
                string[] files = null;
                try
                {
                    files = Directory.GetFiles(path);
                }
                catch (Exception ex)
                {
                    Console.Error.WriteLine(ex);
                }
                if (files != null)
                {
                    for (int i = 0; i < files.Length; i++)
                    {
                        yield return files[i];
                    }
                }
            }
        }
        
        private string docLibraryName = "";

        private void button1_Click(object sender, EventArgs e)
        {
            string[] dirInfos = Directory.GetDirectories(saveLocation.Text);

            float folderSize = CalculateFolderSize(saveLocation.Text);
            updateProgress((int)folderSize, true);

            statusBox.AppendText(dirInfos.Length.ToString() + " Folders found.\n");

            Uri site = new Uri(targetUrl.Text);
            string webServicePath = "_vti_bin/Sites.asmx";
            string webServiceUrl = Path.Combine(site.AbsoluteUri, webServicePath);

            siteService.Url = webServiceUrl;


            foreach (string d in dirInfos)
            {
                int loc = d.LastIndexOf("\\");
                string folderName = d.Remove(0, (loc + 1));
                statusBox.AppendText("Creating Subsite: " + folderName + "...");

                // Create Subsite:
                string siteUrl = "";
                string siteTitle = folderName;
                string siteTemplate = "";

                switch (templateType.Text)
                {
                    case "Proposal Template":
                        siteUrl = folderName.Remove(4).ToLower();
                        siteTitle = "P" + siteTitle;
                        siteTemplate = "{3FBFD078-8EC7-4265-9F15-A99F0E165E85}#iGAS Project Template";
                        docLibraryName = "Project Control Documents";
                        break;
                    case "Project Template":
                        siteUrl = folderName.Remove(4).ToLower();
                        siteTemplate = "{3FBFD078-8EC7-4265-9F15-A99F0E165E85}#iGAS Project Template";
                        docLibraryName = "Project Control Documents";
                        break;
                    case "Support Template":
                        siteUrl = folderName.Replace(" ", "_").ToLower();
                        siteTemplate = "{9B3BBA6B-4239-4E2F-995C-FE961006CEFC}#Support Template";
                        docLibraryName = "Support Files";
                        break;
                }

                // To add a new template browse to: https://igas.sharepoint.com/_layouts/templatepick.aspx and then choose custom then inspect DOM.

                SPSites.CreateWebResponseCreateWebResult result = siteService.CreateWeb(siteUrl, siteTitle, siteTitle, siteTemplate, 1033, true, 1033, true, 1033, true, false, false, false, false, true, true);

                statusBox.AppendText("Done.\n");

                // Update the Copy Service url to the newly created site:
                string copyServicePath = siteUrl + "/_vti_bin/Copy.asmx";
                string copyServiceUrl = Path.Combine(site.AbsoluteUri, copyServicePath);
                copyService.Url = copyServiceUrl;

                // Update the DWS Service url to the newly created site:
                string dwsServicePath = siteUrl + "/_vti_bin/Dws.asmx";
                string dwsServiceUrl = Path.Combine(site.AbsoluteUri, dwsServicePath);
                dwsService.Url = dwsServiceUrl;

                // Walk and upload files/folders:
                WalkDirectoryTree(new DirectoryInfo(d), siteUrl);

                statusBox.AppendText("Site Completed.\n");
            }
        }

        private DateTime tstart = DateTime.Now;
        private TimeSpan diffTime;
        private int dataRemaining = 0;
        private int dataTotal = 0;
        public TimeSpan eta;
        private double downloadSpeed = 0.0;

        private void updateProgress(int size, bool reset = false)
        {
            // Calculate the difference in time since the start:
            diffTime = DateTime.Now - tstart;
            if (reset)
            {
                // We are starting a new backup process so lets reset:
                overallProgress.Maximum = size;
                dataTotal = size;
                dataRemaining = size;
                overallProgress.Value = 0;
                eta = TimeSpan.FromSeconds(0);
            }
            else
            {
                // We're in the process of backing things up so lets update the progress/eta based on what we've done thus far:
                overallProgress.Value += size;
                dataRemaining -= size;
                eta = TimeSpan.FromMilliseconds(dataRemaining / ((dataTotal - dataRemaining) / diffTime.TotalMilliseconds));
                downloadSpeed = (dataRemaining / (1024 * 8)) / eta.TotalSeconds;
            }
            // Set the status bar texts to suit:
            uploadSpeed.Text = string.Format("{0:0.00} Mbps", downloadSpeed);
            overallETA.Text = string.Format("{0:00}:{1:00}:{2:00}", (int)eta.TotalHours, eta.Minutes, eta.Seconds);
            // Refresh the status bar:
            uploadSpeed.Update();
            overallETA.Update();
        }

        private string convertFileSize(uint fileSize)
        {
            string append = "B";
            if (fileSize > 1024)
            {
                fileSize = fileSize / 1024;
                append = "kB";
            }
            if (fileSize > 1024)
            {
                fileSize = fileSize / 1024;
                append = "MB";
            }
            if (fileSize > 1024)
            {
                fileSize = fileSize / 1024;
                append = "GB";
            }

            return (fileSize.ToString() + append);
        }

        void WalkDirectoryTree(System.IO.DirectoryInfo root, string siteUrl, string path = "")
        {
            System.IO.FileInfo[] files = null;
            System.IO.DirectoryInfo[] subDirs = null;

            // Update the current folder path:
            if (path == "")
            {
                path = docLibraryName;
            }
            else
            {
                path = path + "/" + root.Name;
            }
            statusBox.AppendText("Path: " + path + "\n");
            // First, process all the files directly under this folder 
            try
            {
                files = root.GetFiles("*.*");
            }
            // This is thrown if even one of the files requires permissions greater 
            // than the application provides. 
            catch (UnauthorizedAccessException e)
            {
                // This code just writes out the message and continues to recurse. 
                // You may decide to do something different here. For example, you 
                // can try to elevate your privileges and access the file again.
                Console.WriteLine(e.Message);
            }

            catch (System.IO.DirectoryNotFoundException e)
            {
                Console.WriteLine(e.Message);
            }

            if (files != null)
            {
                // Build the tree structure first:
                subDirs = root.GetDirectories();

                foreach (System.IO.DirectoryInfo dirInfo in subDirs)
                {
                    // Generate folder:
                    createFolder(dirInfo, path);
                    // Resursive call for each subdirectory.
                    WalkDirectoryTree(dirInfo, siteUrl, path);
                }

                // Now upload all the files to the folders:
                foreach (System.IO.FileInfo fi in files)
                {
                    // In this example, we only access the existing FileInfo object. If we 
                    // want to open, delete or modify the file, then 
                    // a try-catch block is required here to handle the case 
                    // where the file has been deleted since the call to TraverseTree().
                    //Console.WriteLine(fi.FullName);
                    uploadFile(fi, root, siteUrl);
                }
            }
        }

        protected static float CalculateFolderSize(string folder)
        {
            float folderSize = 0.0f;
            try
            {
                //Checks if the path is valid or not
                if (!Directory.Exists(folder))
                    return folderSize;
                else
                {
                    try
                    {
                        foreach (string file in Directory.GetFiles(folder))
                        {
                            if (System.IO.File.Exists(file))
                            {
                                FileInfo finfo = new FileInfo(file);
                                folderSize += finfo.Length;
                            }
                        }

                        foreach (string dir in Directory.GetDirectories(folder))
                            folderSize += CalculateFolderSize(dir);
                    }
                    catch (NotSupportedException e)
                    {
                        Console.WriteLine("Unable to calculate folder size: {0}", e.Message);
                    }
                }
            }
            catch (UnauthorizedAccessException e)
            {
                Console.WriteLine("Unable to calculate folder size: {0}", e.Message);
            }
            return folderSize;
        }

        void createFolder(System.IO.DirectoryInfo dir, string path)
        {
            statusBox.AppendText("Creating Folder: " + path + "/" + dir.Name + "...");
            string result;
            result = dwsService.CreateFolder(path + "/" + dir.Name);
            statusBox.AppendText("Done.\n");

        }

        void uploadFile(System.IO.FileInfo fi, System.IO.DirectoryInfo root, string siteUrl)
        {
            var localFilePath = fi.FullName;
            var fileName = Path.GetFileName(localFilePath);
            var filePath = localFilePath.Replace(saveLocation.Text + "\\", "");
            filePath = filePath.Replace("\\", "/");
            int loc = filePath.IndexOf("/");
            filePath = filePath.Remove(0, loc + 1);
            Uri site = new Uri(targetUrl.Text);
            string serverUrl = string.Format("{0}/{1}", site.AbsoluteUri, siteUrl);

            var destinationUrl = string.Format("{0}/{1}/{2}", serverUrl, docLibraryName, filePath);

            var fileBytes = System.IO.File.ReadAllBytes(localFilePath);
            var info = new[]{new SPCopy.FieldInformation
                        {
                            DisplayName = fileName,
                            Id = Guid.NewGuid(),
                            InternalName = fileName,
                            Type = SPCopy.FieldType.File,
                            Value = fileName
                        }};

            SPCopy.CopyResult[] results;
            statusBox.AppendText("Uploading: " + destinationUrl + "...");
            copyService.CopyIntoItems(destinationUrl, new[] { destinationUrl }, info, fileBytes, out results);
            updateProgress((int)fi.Length);
            statusBox.AppendText("Done.\n");
        }
    }
}
