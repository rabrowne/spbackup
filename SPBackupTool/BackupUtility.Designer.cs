﻿namespace SPBackupTool
{
    partial class BackupUtility
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.targetUrl = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.connectButton = new System.Windows.Forms.Button();
            this.statusBox = new System.Windows.Forms.TextBox();
            this.saveLocation = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.browseButton = new System.Windows.Forms.Button();
            this.saveLocationDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.label3 = new System.Windows.Forms.Label();
            this.backupButton = new System.Windows.Forms.Button();
            this.checkAll = new System.Windows.Forms.CheckBox();
            this.sitesList = new System.Windows.Forms.ListView();
            this.statusProgressBar = new System.Windows.Forms.ProgressBar();
            this.statusDownloadSpeed = new System.Windows.Forms.Label();
            this.statusDataRemaining = new System.Windows.Forms.Label();
            this.statusETA = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // targetUrl
            // 
            this.targetUrl.Location = new System.Drawing.Point(12, 42);
            this.targetUrl.Name = "targetUrl";
            this.targetUrl.Size = new System.Drawing.Size(458, 20);
            this.targetUrl.TabIndex = 0;
            this.targetUrl.Text = "https://[somesite].sharepoint.com";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(117, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "URL of Sharepoint site:";
            // 
            // connectButton
            // 
            this.connectButton.Location = new System.Drawing.Point(476, 40);
            this.connectButton.Name = "connectButton";
            this.connectButton.Size = new System.Drawing.Size(75, 23);
            this.connectButton.TabIndex = 2;
            this.connectButton.Text = "Connect";
            this.connectButton.UseVisualStyleBackColor = true;
            this.connectButton.Click += new System.EventHandler(this.connectButton_Click);
            // 
            // statusBox
            // 
            this.statusBox.Location = new System.Drawing.Point(563, 26);
            this.statusBox.Multiline = true;
            this.statusBox.Name = "statusBox";
            this.statusBox.ReadOnly = true;
            this.statusBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.statusBox.Size = new System.Drawing.Size(346, 375);
            this.statusBox.TabIndex = 9;
            // 
            // saveLocation
            // 
            this.saveLocation.Enabled = false;
            this.saveLocation.Location = new System.Drawing.Point(12, 80);
            this.saveLocation.Name = "saveLocation";
            this.saveLocation.Size = new System.Drawing.Size(458, 20);
            this.saveLocation.TabIndex = 10;
            this.saveLocation.MouseClick += new System.Windows.Forms.MouseEventHandler(this.saveLocation_MouseClick);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(124, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Select Backup Location:";
            // 
            // browseButton
            // 
            this.browseButton.Enabled = false;
            this.browseButton.Location = new System.Drawing.Point(476, 78);
            this.browseButton.Name = "browseButton";
            this.browseButton.Size = new System.Drawing.Size(75, 23);
            this.browseButton.TabIndex = 12;
            this.browseButton.Text = "Browse";
            this.browseButton.UseVisualStyleBackColor = true;
            this.browseButton.Click += new System.EventHandler(this.browseButton_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 120);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(118, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "Select Sites to Backup:";
            // 
            // backupButton
            // 
            this.backupButton.Enabled = false;
            this.backupButton.Location = new System.Drawing.Point(476, 378);
            this.backupButton.Name = "backupButton";
            this.backupButton.Size = new System.Drawing.Size(75, 23);
            this.backupButton.TabIndex = 15;
            this.backupButton.Text = "Backup";
            this.backupButton.UseVisualStyleBackColor = true;
            this.backupButton.Click += new System.EventHandler(this.backupButton_Click);
            // 
            // checkAll
            // 
            this.checkAll.AutoSize = true;
            this.checkAll.Location = new System.Drawing.Point(133, 120);
            this.checkAll.Name = "checkAll";
            this.checkAll.Size = new System.Drawing.Size(70, 17);
            this.checkAll.TabIndex = 21;
            this.checkAll.Text = "Select All";
            this.checkAll.UseVisualStyleBackColor = true;
            this.checkAll.CheckedChanged += new System.EventHandler(this.checkAll_CheckedChanged);
            // 
            // sitesList
            // 
            this.sitesList.CheckBoxes = true;
            this.sitesList.Location = new System.Drawing.Point(12, 137);
            this.sitesList.Name = "sitesList";
            this.sitesList.Size = new System.Drawing.Size(539, 229);
            this.sitesList.TabIndex = 22;
            this.sitesList.UseCompatibleStateImageBehavior = false;
            this.sitesList.View = System.Windows.Forms.View.List;
            // 
            // statusProgressBar
            // 
            this.statusProgressBar.Location = new System.Drawing.Point(12, 378);
            this.statusProgressBar.Name = "statusProgressBar";
            this.statusProgressBar.Size = new System.Drawing.Size(311, 23);
            this.statusProgressBar.TabIndex = 23;
            // 
            // statusDownloadSpeed
            // 
            this.statusDownloadSpeed.AutoSize = true;
            this.statusDownloadSpeed.BackColor = System.Drawing.Color.Transparent;
            this.statusDownloadSpeed.Location = new System.Drawing.Point(329, 383);
            this.statusDownloadSpeed.Name = "statusDownloadSpeed";
            this.statusDownloadSpeed.Size = new System.Drawing.Size(42, 13);
            this.statusDownloadSpeed.TabIndex = 24;
            this.statusDownloadSpeed.Text = "-- Mbps";
            // 
            // statusDataRemaining
            // 
            this.statusDataRemaining.AutoSize = true;
            this.statusDataRemaining.BackColor = System.Drawing.Color.Transparent;
            this.statusDataRemaining.Location = new System.Drawing.Point(386, 383);
            this.statusDataRemaining.Name = "statusDataRemaining";
            this.statusDataRemaining.Size = new System.Drawing.Size(20, 13);
            this.statusDataRemaining.TabIndex = 25;
            this.statusDataRemaining.Text = "0B";
            // 
            // statusETA
            // 
            this.statusETA.AutoSize = true;
            this.statusETA.BackColor = System.Drawing.Color.Transparent;
            this.statusETA.Location = new System.Drawing.Point(421, 383);
            this.statusETA.Name = "statusETA";
            this.statusETA.Size = new System.Drawing.Size(49, 13);
            this.statusETA.TabIndex = 26;
            this.statusETA.Text = "00:00:00";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(921, 24);
            this.menuStrip1.TabIndex = 29;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(92, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // BackupUtility
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(921, 412);
            this.Controls.Add(this.statusETA);
            this.Controls.Add(this.statusDataRemaining);
            this.Controls.Add(this.statusDownloadSpeed);
            this.Controls.Add(this.statusProgressBar);
            this.Controls.Add(this.checkAll);
            this.Controls.Add(this.backupButton);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.browseButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.saveLocation);
            this.Controls.Add(this.statusBox);
            this.Controls.Add(this.connectButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.targetUrl);
            this.Controls.Add(this.sitesList);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "BackupUtility";
            this.Text = "iGAS Sharepoint Backup Utility";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox targetUrl;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button connectButton;
        private System.Windows.Forms.TextBox statusBox;
        private System.Windows.Forms.TextBox saveLocation;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button browseButton;
        private System.Windows.Forms.FolderBrowserDialog saveLocationDialog;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button backupButton;
        private System.Windows.Forms.CheckBox checkAll;
        private System.Windows.Forms.ListView sitesList;
        private System.Windows.Forms.ProgressBar statusProgressBar;
        private System.Windows.Forms.Label statusDownloadSpeed;
        private System.Windows.Forms.Label statusDataRemaining;
        private System.Windows.Forms.Label statusETA;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
    }
}

